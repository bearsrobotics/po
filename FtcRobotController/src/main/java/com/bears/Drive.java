package com.bears;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Gamepad;
import java.lang.Math;

/**
 * Awesomely created by lord_alonso on 5/09/15.
 */
public class Drive {
    private DcMotor rightFront;
    private DcMotor rightBack;
    private DcMotor leftFront;
    private DcMotor leftBack;
    private Gamepad gamepad;

    private float xPower;
    private float yPower;
    private float turnPower;

    private float joyY;
    private float joyX;
    private float joyY2;
    private float joyX2;

    private float[] tInputs;
    private float[] tOutputs;

    private float deadband;
    private boolean smoothModeEnabled;
    private boolean scaleModeEnabled;
    private float precisionMultiplier;
    private float currentPrecisionMultiplier;

    public Drive(DcMotor rightFront, DcMotor rightBack, DcMotor leftFront, DcMotor leftBack, Gamepad gamepad){
        this(rightFront, rightBack, leftFront, leftBack, gamepad,(float)0.0,false,false);
    }

    public Drive(DcMotor rightFront, DcMotor rightBack, DcMotor leftFront, DcMotor leftBack, Gamepad gamepad, float deadband, boolean smooth, boolean scale){
        this.rightFront = rightFront;
        this.rightBack = rightBack;
        this.leftFront= leftFront;
        this.leftBack = leftBack;

        this.leftFront.setDirection(DcMotor.Direction.REVERSE);
        this.leftBack.setDirection(DcMotor.Direction.REVERSE);

        this.gamepad = gamepad;
        this.deadband = deadband;
        this.smoothModeEnabled = smooth;
        this.scaleModeEnabled = scale;
        this.precisionMultiplier = (float)0.5;
        this.currentPrecisionMultiplier = 1;
    }


    public void drive(float yPower, float xPower, float turnPower){
        float[] outputs = new float[4];
        tOutputs=outputs;
        outputs[0] = yPower - xPower - turnPower;
        outputs[1] = yPower + xPower - turnPower;
        outputs[2] = yPower + xPower + turnPower;
        outputs[3] = yPower - xPower + turnPower;
        tuneOutputs(outputs);

        rightFront.setPower(outputs[0]);
        rightBack.setPower(outputs[1]);
        leftFront.setPower(outputs[2]);
        leftBack.setPower(outputs[3]);
    }
    /*----------------------------------------Drives---------------------------------------------*/

    public void tankdrive(){
        updateJoysticks();
        float[] inputs = {joyY, joyY2, 0};
        tuneInputs(inputs);
        tInputs=inputs;

        yPower = inputs[0]+inputs[1];                 //
        xPower = (float)0.0;                          // MUST SCALE!
        turnPower = inputs[0]-inputs[1];              //

        scaleModeEnabled = true;
        drive(yPower, xPower, turnPower);
    }

    public void arcadeDrive(){
        leftFront.setPower(.5);

        updateJoysticks();
        float[] inputs ={joyY,joyX2, 0};
        tuneInputs(inputs);
        tInputs=inputs;

        rightFront.setPower(.5);

        yPower = inputs[0];
        xPower = (float)0.0;
        turnPower = inputs[1];

        drive(yPower,xPower,turnPower);
    }

    public void omniDrive(){
        updateJoysticks();
        float[] inputs ={joyY,joyX,joyX2};
        tuneInputs(inputs);
        tInputs=inputs;

        yPower = inputs[0];
        xPower = inputs[1];
        turnPower = inputs[2];

        drive(yPower, xPower, turnPower);
    }

    /*-----------------------------------Input & Output Management-----------------------------------------*/

    private void updateJoysticks(){

        joyY = -gamepad.left_stick_y;
        joyX = gamepad.left_stick_x;
        joyY2 = -gamepad.right_stick_y;
        joyX2 = gamepad.right_stick_x;
    }

    private void tuneInputs(float[] inputs){
        tuneDeadband(inputs);
        if (smoothModeEnabled)
            tuneSmooth(inputs);
    }

    private void tuneOutputs(float[] outputs){
        if(scaleModeEnabled)
            scale(outputs);
        tunePrecision(outputs);
    }



    private void tuneDeadband(float[] inputs) {
        for (int i=0; i < inputs.length; i++) {
            int sig = (int) Math.signum(inputs[i]);
            if (Math.abs(inputs[i]) < deadband)
                inputs[i] = 0;
            else
                inputs[i] = (inputs[i] - deadband * sig)  / (1 - deadband);
        }
    }


    private void tuneSmooth(float[] inputs){
        for (int i=0; i < inputs.length; i++)
            inputs[i] = (float) Math.pow(inputs[i], 2);
    }


    private void tunePrecision(float[] outputs){
        for (int i=0; i < outputs.length; i++)
            outputs[i] = outputs[i] * currentPrecisionMultiplier;
    }


    private void scale(float[] outputs){                   // Beta Function
        float currentLargest = 0;
        for (float output : outputs) {                     //
            if (Math.abs(output) > currentLargest)         // Get largest absolute value in output
                currentLargest = Math.abs(output);         //
        }                                                  //

        if (currentLargest != 0 && currentLargest > 1.0) {
            for (int i = 0; i < outputs.length; i++) {     //
                outputs[i] = outputs[i] / currentLargest;  // Scale all other values so max is 1
            }                                              //
        }
    }



/*____________________________________Getters and Setters________________________________________*/


    //Setter and Getter methods for Smooth(squared) Mode
    public boolean getSmoothModeEnabled() {
        return smoothModeEnabled;
    }

    public void setSmoothModeEnabled(boolean smoothModeEnabled) {
        this.smoothModeEnabled = smoothModeEnabled;
    }

    //Setter and Getter methods for Scale Mode
    public boolean getScaleModeEnabled() {
        return scaleModeEnabled;
    }

    public void setScaleModeEnabled(boolean scaleModeEnabled) {
        this.scaleModeEnabled = scaleModeEnabled;
    }

    //Setter and Getter methods for Deadband
    public float getDeadband() {
        return deadband;
    }

    public void setDeadband(float deadband) {
        this.deadband = deadband;
    }

    //Setter and Getter methods for Precision Multiplier
    public float getPrecisionMultiplier() {
        return precisionMultiplier;
    }

    public void setPrecisionMultiplier(float precisionMultiplier) {
        this.precisionMultiplier = precisionMultiplier;
    }

    //Getter method for Precision Current Multiplier
    public float getCurrentPrecisionMultiplier(){
        return currentPrecisionMultiplier;
    }

    //Toggler method for Precision
    public void togglePrecision(){
        currentPrecisionMultiplier = -currentPrecisionMultiplier + precisionMultiplier + 1;
    }


    public float getxPower() {
        return xPower;
    }

    public float getyPower() {
        return yPower;
    }

    public float getTurnPower() {
        return turnPower;
    }

    public float getJoyY() {
        return joyY;
    }

    public float getJoyX() {
        return joyX;
    }

    public float getJoyY2() {
        return joyY2;
    }

    public float getJoyX2() {
        return joyX2;
    }


    public String toString(){
        return String.format("X Power: %f\n", xPower)+
                String.format("Y Power: %f\n", yPower)+
                String.format("Turn Power: %f\n", turnPower)+
                String.format("Inputs 0: %f\n", tInputs[0])+
                String.format("Inputs 1: %f\n", tInputs[1])+
                String.format("Inputs 2: %f\n", tInputs[2])+
                String.format("Outputs 0: %f\n", tOutputs[0])+
                String.format("Outputs 1: %f\n", tOutputs[1])+
                String.format("Outputs 2: %f\n", tOutputs[2])+
                String.format("Outputs 3: %f\n", tOutputs[3])+
                String.format("Joy Y: %f\n", joyY)+
                String.format("Joy X: %f\n", joyX)+
                String.format("Joy Y2: %f\n", joyY2)+
                String.format("Joy X2: %f\n", joyX2);
    }
}
