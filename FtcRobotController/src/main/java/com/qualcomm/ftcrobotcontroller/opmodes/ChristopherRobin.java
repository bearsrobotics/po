package com.qualcomm.ftcrobotcontroller.opmodes;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.bears.Drive;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.Range;

/**
 * Awesomely created by Bears on 07/11/2015.
 */

public class ChristopherRobin extends OpMode{
    private float joyY;
    private float joyX;

    DcMotor rightFront;
    DcMotor rightBack;
    DcMotor leftFront;
    DcMotor leftBack;
    DcMotor espiropopa;
    DcMotor banda;

    Servo palanca1;
    Servo palanca2;

    //Drive robotDrive;

    boolean servoChange = true;

    @Override
    public void init() {
        rightFront = hardwareMap.dcMotor.get("rightFront");
        rightBack = hardwareMap.dcMotor.get("rightBack");
        leftFront = hardwareMap.dcMotor.get("leftFront");
        leftBack = hardwareMap.dcMotor.get("leftBack");
        espiropopa = hardwareMap.dcMotor.get("espiropopa");
        banda = hardwareMap.dcMotor.get("banda");

        rightBack.setDirection(DcMotor.Direction.REVERSE);
        rightFront.setDirection(DcMotor.Direction.REVERSE);

        palanca1 = hardwareMap.servo.get("palanca1");
        palanca2 = hardwareMap.servo.get("palanca2");

        //robotDrive = new Drive(rightFront, rightBack, leftFront, leftBack, gamepad1);
        //robotDrive.setDeadband((float)0.15);
        //robotDrive.setSmoothModeEnabled(true);
        //robotDrive.setScaleModeEnabled(true);

        palanca1.setPosition(0);
        palanca2.setPosition(1);
    }

    @Override
    public void loop() {


        if (Math.abs(gamepad1.left_stick_y)<.1)
            joyY=0;
        else {
            joyY = -gamepad1.left_stick_y;
            joyY = (float) ( (joyY - .1 * Math.signum(joyY))  / (1 - .1) );
        }

        if (Math.abs(gamepad1.right_stick_x)<.1)
            joyX=0;
        else {
            joyX = gamepad1.right_stick_x;
            joyX = (float) ( (joyX - .1 * Math.signum(joyX))  / (1 - .1) );
        }

        joyX = (float) Math.pow(joyX, 2) * Math.signum(joyX);
        joyY = (float) Math.pow(joyY, 2) * Math.signum(joyY);


        double right = Range.clip(joyY - joyX, -1, 1);
        double left = Range.clip(joyY + joyX, -1, 1);

        rightFront.setPower(right);
        rightBack.setPower(right);
        leftFront.setPower(left);
        leftBack.setPower(left);

        //robotDrive.arcadeDrive();

        if(gamepad2.right_bumper){
            espiropopa.setPower(1);
        }
        else if (gamepad2.right_trigger > 0.1) {
            espiropopa.setPower(-1);
        }
        else{
            espiropopa.setPower(0);
        }


        if(gamepad2.left_bumper){
            banda.setPower(1);
        }0
        else if (gamepad2.left_trigger > 0.1) {
            banda.setPower(-1);
        }
        else{
            banda.setPower(0);
        }


        if(gamepad2.start)
        {
            if(servoChange){
                if (palanca1.getPosition()>.8){
                    palanca1.setPosition(0);
                    palanca2.setPosition(1);
                }
                else{
                    palanca1.setPosition(.9);
                    palanca2.setPosition(.1);
                }
                servoChange=false;
            }
        }
        else{
            servoChange=true;
        }

        //telemetry.addData("00", robotDrive.toString());
        telemetry.addData("Joy Y", gamepad1.left_stick_y);
        telemetry.addData("Power", joyY);
        telemetry.addData("servo change", servoChange);
        telemetry.addData("palanca1", palanca1.getPosition());
        telemetry.addData("palanca2", palanca2.getPosition());
        //telemetry.addData("00", robotDrive.toString());



    }
}
