package com.qualcomm.ftcrobotcontroller.opmodes;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Gamepad;
import com.qualcomm.robotcore.util.Range;

/**
 * Created by Bears on 07/11/2015.
 */
public class Santol extends OpMode{
       DcMotor rightFront;
    DcMotor rightBack;
    DcMotor leftFront;
    DcMotor leftBack;
    DcMotor espiropapa;
    DcMotor banda;

    @Override
    public void init() {
        rightFront = hardwareMap.dcMotor.get("rightFront");
        rightBack = hardwareMap.dcMotor.get("rightBack");
        leftFront = hardwareMap.dcMotor.get("leftFront");
        leftBack = hardwareMap.dcMotor.get("leftBack");
        espiropapa = hardwareMap.dcMotor.get("espiropapa");
        banda = hardwareMap.dcMotor.get("banda");
    }

    @Override
    public void loop() {

        double joyY = -gamepad1.left_stick_y;
        double joyX = gamepad1.right_stick_x;


        if(Math.abs(gamepad1.left_stick_y) < 0.10){
            joyY=0;
        }
        if(Math.abs(gamepad1.right_stick_x) < 0.10){
            joyX=0;
        }


        joyY= Math.pow(joyY, 2) * Math.signum(joyY);
        joyX= Math.pow(joyX, 2) * Math.signum(joyX);


        double right = (joyY - joyX);

        right = Range.clip(right, -1, 1);

        rightFront.setPower(right);
        rightBack.setPower(right);
        leftBack.setPower(joyY + joyX);
        leftFront.setPower(joyY + joyX);

        if(gamepad1.start){
            espiropapa.setPower(1);
        }
        else{
            espiropapa.setPower(0);
        }



    }
}
