package com.qualcomm.ftcrobotcontroller.opmodes;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;

/**
 * Awesomely created by Bears on 14/11/2015.
 */
public class Po extends OpMode {

    DcMotor rightFront;
    DcMotor rightBack;
    DcMotor leftFront;
    DcMotor leftBack;
    DcMotor espiropapa;
    DcMotor banda;
    Servo palanca1;
    Servo palanca2;



    @Override
    public void init() {
        rightFront = hardwareMap.dcMotor.get("rightFront");
        rightBack = hardwareMap.dcMotor.get("rightBack");
        leftFront = hardwareMap.dcMotor.get("leftFront");
        leftBack = hardwareMap.dcMotor.get("leftBack");
        espiropapa = hardwareMap.dcMotor.get("espiropapa");
        banda = hardwareMap.dcMotor.get("banda");
        palanca1 = hardwareMap.servo.get("palanca1");
        palanca2 = hardwareMap.servo.get("palanca2");

    }

    @Override
    public void loop() {

        rightFront.setPower(-gamepad1.left_stick_y);
        rightBack.setPower(-gamepad1.left_stick_y);
        leftFront.setPower(-gamepad1.left_stick_y);
        leftBack.setPower(-gamepad1.left_stick_y);

    }
}
