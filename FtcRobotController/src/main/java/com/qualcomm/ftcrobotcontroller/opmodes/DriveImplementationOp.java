package com.qualcomm.ftcrobotcontroller.opmodes;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.bears.Drive;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Gamepad;

/**
 * Awesomely created by lord_alonso on 6/09/15.
 */
public class DriveImplementationOp extends OpMode {
    DcMotor rightFront;
    DcMotor rightBack;
    DcMotor leftFront;
    DcMotor leftBack;
    Gamepad gamepad1;
    Gamepad gamepad2;

    Drive robotDrive;

    boolean xReset;

    @Override
    public void init() {
        rightFront = hardwareMap.dcMotor.get("rightFront");
        rightBack = hardwareMap.dcMotor.get("rightBack");
        leftFront = hardwareMap.dcMotor.get("leftFront");
        leftBack = hardwareMap.dcMotor.get("leftBack");

        gamepad1= new Gamepad();
        gamepad2 = new Gamepad();

        robotDrive = new Drive(rightFront, rightBack, leftBack, leftFront, gamepad1);
        robotDrive.setDeadband((float)0.1);
        robotDrive.setSmoothModeEnabled(true);
        robotDrive.setScaleModeEnabled(true);

        xReset = true;
    }

    @Override
    public void loop() {

        //robotDrive.tankdrive();
        robotDrive.arcadeDrive();
        //robotDrive.omniDrive();

        if(gamepad1.x){
            if(xReset) {
                robotDrive.togglePrecision();
                xReset = false;
            }
        }
        else{
            xReset = true;
        }

        telemetry.addData("Drive", robotDrive.toString());

    }
}
